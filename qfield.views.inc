<?php

/**
 * Implementation of hook_views_data().
 */
function qfield_views_data() {
  $data['qfield_response']['table']['group']  = t('Response');
  $data['qfield_response']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Response'),
    'help' => t("Question fields response, can be related to the node the question belongs to and user who have answered the question."),
    'weight' => -10,
  );
  $data['qfield_response']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['qfield_response']['uid'] = array(
    'title' => t('Responder'),
    'help' => t('User who have given this response.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Responder'),
    ),
  );
  $data['qfield_response']['field_name'] = array(
    'title' => t('Field'),
    'help' => t('Name of field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
       'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['qfield_response']['field_delta'] = array(
    'title' => t('Question field delta'),
    'help' => t('The sequence number for a question field if multiple values are enalbed.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['qfield_response']['nid'] = array(
    'title' => t('Question node'),
    'help' => t('Node that contains the question this response was written for.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );
  $data['qfield_response']['tag'] = array(
    'title' => t('Tag'),
    'help' => t('Tag to group values.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
       'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['qfield_response']['value'] = array(
    'title' => t('Value'),
    'help' => t('User answer value.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['qfield_response']['value_rendered'] = array(
    'field' => array(
      'title' => t('Rendered value'),
      'help' => t('User answer rendered value.'),
      'handler' => 'views_handler_field_qfield_response_value_rendered',
    ),
  );
  $data['qfield_response']['question'] = array(
    'field' => array(
      'title' => t('Question'),
      'help' => t('A question this response was submitted for.'),
      'handler' => 'views_handler_field_qfield_response_question',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function qfield_views_data_alter(&$data) {

}

/**
 * Implementation of hook_views_handlers().
 */
function qfield_views_handlers() {
  return array(
    'handlers' => array(
      'views_handler_field_qfield_response_value_rendered' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_qfield_response_question' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
