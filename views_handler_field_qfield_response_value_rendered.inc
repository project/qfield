<?php

/**
 * Field handler to present a rendered value for a question.
 */
class views_handler_field_qfield_response_value_rendered extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['field_name'] = 'field_name';
    $this->additional_fields['field_delta'] = 'field_delta';
    $this->additional_fields['value'] = 'value';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $node = node_load($values->{$this->aliases['nid']});
    $field_name = $values->{$this->aliases['field_name']};
    $field_info = content_fields($field_name, $node->type);
    $delta = $values->{$this->aliases['field_delta']};
    $item = $node->{$field_name}[$delta];
    return theme('qfield_response', $node, $field_info, $item, $delta, $values->{$this->aliases['value']});
  }
}
