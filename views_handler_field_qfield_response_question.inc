<?php

/**
 * Field handler to present the question the response was submitted for.
 */
class views_handler_field_qfield_response_question extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['field_name'] = 'field_name';
    $this->additional_fields['field_delta'] = 'field_delta';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $node = node_load($values->{$this->aliases['nid']});
    $field_name = $values->{$this->aliases['field_name']};
    $delta = $values->{$this->aliases['field_delta']};
    $item = $node->{$field_name}[$delta];
    return check_plain($item['question']);
  }
}
