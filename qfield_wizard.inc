<?php


/**
 * @file
 * Wizard implementation for the Question Field module. 
 */

/**
 * Wrapper to qfield questions forms.
 */
function qfield_wizard_form(&$form, &$form_state) {
  $step = $form_state['step'];
  $field_properties = $form_state['field_properties'][$step];
  $info = qfield_question_type($field_properties['item']['question_type']);

  $form['question'] = array(
    '#value' => theme('qfield_question', $field_properties['item']['safe']),
    '#weight' => -100,
  );

  // Default 'base' to field type itself.
  $base = isset($info['base']) ? $info['base'] : $field_properties['item']['question_type'];

  $function = $info['module'] . '_qfield_' . $base . '_form';
  if (function_exists($function)) {
    $result = $function($field_properties['field_node'], $field_properties['field'], $field_properties['item'], $field_properties['delta'], $form_state['data'][$step]);
    if (isset($result) && is_array($result)) {
      $form += $result;
    }
  }
}

/**
 * Submit a single wizard form. Store data for later saving it.
 */
function qfield_wizard_form_submit(&$form, &$form_state) {
  $step = $form_state['step'];
  $form_state['data'][$step] = $form_state['field_properties'][$step] + array(
    'values' => $form_state['values'],
  );
}

/**
 * Validates a single wizard form.
 */
function qfield_wizard_form_validate(&$form, &$form_state) {
  $step = $form_state['step'];
  $item = $form_state['data'][$step]['item'];
  $info = qfield_question_type($item['question_type']);

  // Default 'base' to field type itself.
  $base = isset($info['base']) ? $info['base'] : $item['question_type'];

  $function = $info['module'] . '_qfield_' . $base . '_form_validate';
  if (function_exists($function)) {
    $form = array();
    $function($form, $form_state['data'][$step]);
  }
}

/**
 * Get the cached changes to a given task handler.
 */
function qfield_wizard_get_cache($name) {
  ctools_include('object-cache');
  return ctools_object_cache_get('qfield_wizard', $name);
}

/**
 * Store changes to a task handler in the object cache.
 */
function qfield_wizard_set_cache($name, $data) {
  ctools_include('object-cache');
  ctools_object_cache_set('qfield_wizard', $name, $data);
}

/**
 * Remove an item from the object cache.
 */
function qfield_wizard_clear_cache($name) {
  ctools_include('object-cache');
  ctools_object_cache_clear('qfield_wizard', $name);
}

/**
 * Callback generated when the add page process is finished.
 */
function qfield_wizard_finish(&$form_state) {
  global $user;

  $data = &$form_state['data'];

  // Save data.
  foreach ($data as $step => $stored_form_state) {
    $item = $stored_form_state['item'];
    $info = qfield_question_type($item['question_type']);

    // Default 'base' to field type itself.
    $base = isset($info['base']) ? $info['base'] : $item['question_type'];

    $function = $info['module'] . '_qfield_' . $base . '_form_submit';
    if (function_exists($function)) {
      $form = array();

      // Unset wizard data.
      unset($stored_form_state['values']['form_token'], $stored_form_state['values']['form_id'], $stored_form_state['values']['form_build_id']);

      // Call submit hook on field type module.
      $value = $function($form, $stored_form_state);

      // Store response.
      $response = array(
        'nid' => $stored_form_state['field_node']->nid,
        'field_name' => $stored_form_state['field']['field_name'],
        'field_delta' => $stored_form_state['delta'],
        'uid' => $user->uid,
        'value' => $value,
      );
      drupal_write_record('qfield_response', $response);
    }
  }

  // Clear the cache
  qfield_wizard_clear_cache($form_state['cache name']);

  $form_state['redirect'] = 'node/' . $stored_form_state['field_node']->nid;
}

/**
 * Callback generated when the 'next' button is clicked.
 *
 * All we do here is store the cache.
 */
function qfield_wizard_next(&$form_state) {
  // Update the cache with changes.
  qfield_wizard_set_cache($form_state['cache name'], $form_state['data']);
}

/**
 * Callback generated when the 'cancel' button is clicked.
 *
 * All we do here is clear the cache.
 */
function qfield_wizard_cancel(&$form_state) {
  // Update the cache with changes.
  qfield_wizard_clear_cache($form_state['cache name']);
}
